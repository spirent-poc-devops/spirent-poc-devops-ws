#!/bin/sh

git clone git@gitlab.com:spirent-poc-devops/service-sample-java.git
git clone git@gitlab.com:spirent-poc-devops/image-gitlabrunner-docker.git
git clone git@gitlab.com:spirent-poc-devops/image-springboot-docker.git
git clone git@gitlab.com:spirent-poc-devops/image-jre-docker.git
git clone git@gitlab.com:spirent-poc-devops/script-cicd-ps.git
git clone git@gitlab.com:spirent-poc-devops/script-baseline-ps.git

git clone git@gitlab.com:spirent-poc-devops/env-dev-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-test-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-onprem-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-cloudaws-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-dev2-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-dev2-python.git
git clone git@gitlab.com:spirent-poc-devops/env-test2-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-test2-python.git
git clone git@gitlab.com:spirent-poc-devops/env-onprem2-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-onprem2-python.git
git clone git@gitlab.com:spirent-poc-devops/env-cloudaws2-ps.git
git clone git@gitlab.com:spirent-poc-devops/env-cloudaws2-python.git

git clone git@gitlab.com:spirent-poc-devops/package-sampleapp.git
git clone git@gitlab.com:spirent-poc-devops/package-sampleapp2.git
git clone git@gitlab.com:spirent-poc-devops/package-sampleapp3.git
git clone git@gitlab.com:spirent-poc-devops/script-appdeploy-ps.git
git clone git@gitlab.com:spirent-poc-devops/package-infraservices-helm.git